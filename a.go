package main

import (
	"bitbucket.org/zyguan/modb"
	"bitbucket.org/zyguan/modc"
)

func main() {
	modc.Hello("Alice")
	modb.Bob()
}
